import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet } from 'react-native';
import { Container, Header, Left, Icon, Body, Right, Content, Text, Footer, FooterTab, Button } from 'native-base';


export default function App() {
  return (
    <Container>
      <Header padder style={styles.header_footer}>
        <Left>
          <Icon style={styles.settings} name="settings"></Icon>
        </Left>
        <Body>

        </Body>
        <Right>

        </Right>
      </Header>
      <Content padder style={styles.content}>
        
          <Button>
          <Text>Create List</Text>
            <Icon type="AntDesign" name="check"></Icon>
          
          </Button>
        
        
      </Content>
      <Footer>
        <FooterTab style={styles.header_footer}>
          <Button full>
            <Text>Test</Text>
          </Button>
        </FooterTab>
      </Footer>
    </Container>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  header_footer: {
    backgroundColor: '#303030',
  },
  content: {
    backgroundColor: '#181818',
  },
  settings:{
    color: 'white',
  },
});
